import { Request, Response } from "express";
import { CreateSpecificationUseCase } from "./CreateSpecificationUseCase";

class CreateSpecificationController {
  constructor(private createSpecificationUseCase: CreateSpecificationUseCase) {}

  handle(request: Request, response: Response): Response {
    const { name, description } = request.body;
    const data = { name, description };

    this.createSpecificationUseCase.execute(data);

    return response.status(201).json(data);
  }
}

export { CreateSpecificationController };
