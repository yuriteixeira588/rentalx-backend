"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var express = require("express");
var routes_1 = require("./routes");
var app = express();
app.get('/', routes_1.createCourse);
app.listen(3333);
